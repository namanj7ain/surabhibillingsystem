#README


    for admin apis - 
        1. /admin/login - for login of admin. In admin we are going to pass name and password as parameter in login
        2. /admin/thisDayBill - for getting all the bills of that day
        3. /admin/thisMonthBill - for getting this current month bill
        4. /admin/logout - for logout of Admin
        5. /admin/getUser/{id} - for getting user by id
        5. /admin/updateUser/update - for updating any user
        6. /admin/addUser - for adding any new user
        7. /admin/deleteUser/{id} - for deleting the existing user with id
    
    for user apis - 
        1. /user/login - for login of user
        2. /user/order - for getting the ids of menus from user. we have to pass the ids of menu in list format
        3. /user/logOut - for logout of the user 
        4. /user/register - for register the new user
        5. /user/menu - for showing the menu to the user
        6. /user/getUser - for getting all the users
