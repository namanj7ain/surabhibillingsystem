CREATE TABLE `restaurantbilling`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

INSERT INTO `restaurantbilling`.`user` (`id`, `name`, `password`) VALUES ('1', 'Ram', 'Ram');
INSERT INTO `restaurantbilling`.`user` (`id`, `name`, `password`) VALUES ('2', 'Deepak', 'Deepak');

CREATE TABLE `restaurantbilling`.`admin` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

INSERT INTO `restaurantbilling`.`admin` (`id`, `name`, `password`) VALUES ('1', 'Atul', 'Atul');
INSERT INTO `restaurantbilling`.`admin` (`id`, `name`, `password`) VALUES ('2', 'Karishma', 'Karishma');

CREATE TABLE `restaurantbilling`.`bill` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `datetime` DATETIME NULL,
  `price` INT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `restaurantbilling`.`menu` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `itemName` VARCHAR(45) NULL,
  `price` INT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `restaurantbilling`.`menu` (`id`, `itemname`, `price`) VALUES ('1', 'Pani Puri', '50');
INSERT INTO `restaurantbilling`.`menu` (`id`, `itemname`, `price`) VALUES ('2', 'Chole Kulche', '130');
INSERT INTO `restaurantbilling`.`menu` (`id`, `itemname`, `price`) VALUES ('3', 'Chowmin', '80');
INSERT INTO `restaurantbilling`.`menu` (`id`, `itemname`, `price`) VALUES ('4', 'Chaat', '90');
INSERT INTO `restaurantbilling`.`menu` (`id`, `itemname`, `price`) VALUES ('5', 'Chole Bhature', '120');
INSERT INTO `restaurantbilling`.`menu` (`id`, `itemname`, `price`) VALUES ('6', 'Khachori', '70');
INSERT INTO `restaurantbilling`.`menu` (`id`, `itemname`, `price`) VALUES ('7', 'Naan Dal', '140');
INSERT INTO `restaurantbilling`.`menu` (`id`, `itemname`, `price`) VALUES ('8', 'Malai Chaap', '180');
INSERT INTO `restaurantbilling`.`menu` (`id`, `itemname`, `price`) VALUES ('9', 'Sahi Paneer', '150');
INSERT INTO `restaurantbilling`.`menu` (`id`, `itemname`, `price`) VALUES ('10', 'Kadai Paneer', '200');
INSERT INTO `restaurantbilling`.`menu` (`id`, `itemname`, `price`) VALUES ('11', 'Naan', '40');
INSERT INTO `restaurantbilling`.`menu` (`id`, `itemname`, `price`) VALUES ('12', 'Roti', '25');

