package com.surabhi.restaurantBilling.Controller;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.surabhi.restaurantBilling.Services.UserService;
import com.surabhi.restaurantBilling.model.Bill;
import com.surabhi.restaurantBilling.model.Menu;
import com.surabhi.restaurantBilling.model.User;

@RestController
@RequestMapping("/user")
public class UserController {

	public static boolean logIn = false;
	
	@Autowired
	private UserService userService;	
	
	@Autowired
	public UserController(UserService theUserService) {
		userService=theUserService;
	}
	
	@GetMapping("/getUser")
	public List<User> findAll(){
		return userService.findAll();
	}
	
	@PostMapping("/login")
	public String logIn(@RequestBody LinkedHashMap<String, String> hashmap) {
		
		String name = hashmap.get("name");
		String password = hashmap.get("password");
		
		logIn = userService.isValid(name, password);
		if(logIn==true)
			return "User loggedIn successfully";
		else
			return "User not found" ;
	}
		@PostMapping("/order")
		public String order(@RequestBody List<Integer> orders) {
			
			if(logIn==true) {
				LinkedHashMap<Integer, Integer> prices = new LinkedHashMap();
				
				// get the menu items
				List<Menu> menu = userService.showMenu();
				
				// iterate over all items in menu and update the hashmap
				for(Menu item:menu) {
					prices.put(item.getId(), item.getPrice());
				}
				// intialize the total bill
				int total = 0; 
				// iterate over ordered items and calculate total bill
				for(int id : orders) {
					total += prices.get(id) ;
				}
				//create Bill object with current total_bill
				Bill bill = new Bill(total) ;
				
				//save the bill object
				userService.save(bill) ;
				return "Total bill for your order is Rs." + total;
			}
			else
				return "Login required";
		}
	
	// to logout the user
	@GetMapping("/logOut")
	public String logOut() {
		logIn = false;
		return "User LoggedOut successfully";
	}
	
	// to register the user
	@PostMapping("/register")
	public String register(@RequestBody User user) {
		userService.save(user);
		return user.toString();
	}
	
	// displays Menu
	@GetMapping("/menu")
	public List<Menu> showMenu(){
		return userService.showMenu();
	}

}
