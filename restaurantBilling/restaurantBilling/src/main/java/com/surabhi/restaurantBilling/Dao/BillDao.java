package com.surabhi.restaurantBilling.Dao;

import com.surabhi.restaurantBilling.model.Bill;

public interface BillDao {

	public void save(Bill bill);
	public int getMonthBill();
	public int getDayBill();
}
