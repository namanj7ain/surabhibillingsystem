package com.surabhi.restaurantBilling.Services;

public interface AdminService {

	public boolean isValid(String username, String password);
	public int getMonthBill();
	public int getDayBill();
}
