package com.surabhi.restaurantBilling.Services;

import java.util.List;

import com.surabhi.restaurantBilling.model.Bill;
import com.surabhi.restaurantBilling.model.Menu;
import com.surabhi.restaurantBilling.model.User;

public interface UserService {

	public List<User> findAll();	
	public boolean isValid(String name, String password);
	public User findById(int id);
	public void save(User user);
	public void update(User user);
	public void deleteById(int id);
	public List<Menu> showMenu();
	public void save(Bill bill);
}
