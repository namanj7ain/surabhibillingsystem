package com.surabhi.restaurantBilling.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.surabhi.restaurantBilling.Dao.BillDao;
import com.surabhi.restaurantBilling.Dao.MenuDao;
import com.surabhi.restaurantBilling.Dao.UserDao;
import com.surabhi.restaurantBilling.model.Bill;
import com.surabhi.restaurantBilling.model.Menu;
import com.surabhi.restaurantBilling.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	@Autowired 
	MenuDao menuDao ;
	
	@Autowired 
	BillDao billDao ;
	
	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return userDao.findall();
	}
	
	@Override
	public void save(User user) {
		userDao.save(user);
	}
	@Override
	public void update(User user) {
		userDao.update(user);
		
	}
	@Override
	public void deleteById(int id) {
		userDao.deleteById(id);
	}
	
	@Override
	public boolean isValid(String name, String password) {
		return userDao.isValid(name, password);
	}
	
	@Override
	public User findById(int id) {
		return userDao.findById(id);
	}
	
	@Override
	public void save(Bill bill) {
		billDao.save(bill);
	}
	
	@Override 
	public List<Menu> showMenu(){
		return menuDao.findAll();
	}
}
