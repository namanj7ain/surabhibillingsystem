package com.surabhi.restaurantBilling.Dao;

import java.util.List;

import com.surabhi.restaurantBilling.model.User;

public interface UserDao {

	public List<User> findall();
	public User findById(int id);
	public void save(User theUser);
	public void deleteById(int theId);
	public boolean isValid(String theName, String thePassword);
	public void update(User theUser);
}
