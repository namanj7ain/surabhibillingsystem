package com.surabhi.restaurantBilling.Controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.surabhi.restaurantBilling.Services.AdminService;
import com.surabhi.restaurantBilling.Services.UserService;
import com.surabhi.restaurantBilling.model.User;

@RestController
@RequestMapping("/admin")
public class AdminController {
	
	// when Admin is login we will make loggedIn = true 
	// and if he make out of the application with logout then we are going to make it logout or false
	// this variable helps us to know whether Admin is logged in or not
	// intially admin will be not logged in
	private static  boolean logIn = false ;

	@Autowired
	UserService userService ;
	
	@Autowired
	AdminService adminService ;
	
	@GetMapping("/thisDayBill")
	public String getDayBill() {
		if(logIn==true)
			return "Today's total bill is " + adminService.getDayBill();
		else
			return "Login required !";
	}
	
	@PostMapping("/login")
	public String logIn(@RequestParam(value="name") String name,@RequestParam(value="password") String password) {
		
		//retrieve data from request body
		//String name = hashmap.get("name");
		//String password = hashmap.get("password");
		
		//check whether admin is valid or not
		logIn = adminService.isValid(name, password);
		if(logIn==true)
			return "Admin loggedIn successfully";
		else
			return "admin not found" ;
	}
	
	@GetMapping("/thisMonthBill")
	public String  getMonthBill() {
		// if admin is login then he will be able to find the bills of this month
		if(logIn==true)
			return "This month's total bill is " + adminService.getMonthBill();
		else 
			return "Login required !";
	}
	
	@GetMapping("/logout")
	public void logOut() {
		logIn = false;
	}
	
	//getting user by id  
		@GetMapping("/getUser/{id}")
		public String getUser(@PathVariable int id) {
			if(logIn==true) {
				User user = userService.findById(id);
				if(user == null) {
					return("user has entered invalid id : "+ id);
				}
				return user.toString();
			}
			else
				return "Login required";
		}
	
	//updating user by id
	@PutMapping("/updateUser/update")
	public String updateUser(@RequestBody User user) {
		if(logIn==true) {
			userService.update(user);
			return user.toString() ;
		}
		else
			return "Login required";	
	}
	
	//for inserting data into user 
	@PostMapping("/addUser")
	public String addUser(@RequestBody User user) {
		
		if(logIn==true) {
			user.setId(0);
			userService.save(user);
			return "user added successfully" ;
		}
		else
			return "Login required !";
	}
		
	//deleting user by id
	@DeleteMapping("/deleteUser/{id}")
	public String deleteUserById(@PathVariable int id) {
		if(logIn==true) {
			User user = userService.findById(id);
			if(user == null) {
				return("user has entered invalid id : "+ id);
			}
			userService.deleteById(id);
			return user.toString();
		}
		else {
			return "Login required";
		}
	}
}


