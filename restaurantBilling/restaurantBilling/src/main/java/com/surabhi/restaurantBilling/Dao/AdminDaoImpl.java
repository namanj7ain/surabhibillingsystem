package com.surabhi.restaurantBilling.Dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.surabhi.restaurantBilling.model.Admin;


@Repository
public class AdminDaoImpl implements AdminDao {

	EntityManager entityManager ;

	@Autowired
	public AdminDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager ;
	}
	
	//-------checks whether given credentials are valid or not and return boolean value----
	@Override
	public boolean isValid(String theName, String thePassword) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		Query theQuery = currentSession.createQuery("from Admin where name = :name and password = :password");
		theQuery.setParameter("name", theName);
		theQuery.setParameter("password", thePassword);
		
		// executes the query
		List<Admin> ls = theQuery.getResultList();
		
		if(ls.size()==0) {// if size of result is zero, it means no records found with given credentials
			return false;
		}
		return true;
	}

}