package com.surabhi.restaurantBilling.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.surabhi.restaurantBilling.Dao.AdminDao;
import com.surabhi.restaurantBilling.Dao.BillDao;

@Service
public class AdminServiceImpl implements AdminService {
	@Autowired
	BillDao billDao;
	
	@Autowired
	AdminDao adminDAO;
	
	@Override
	public int getMonthBill() {
		return billDao.getMonthBill() ;
	}
	
	@Override
	public int getDayBill() {
		return billDao.getDayBill();
	}
	
	@Override
	public boolean isValid(String username, String password) {
		return adminDAO.isValid(username, password) ;
	}
}