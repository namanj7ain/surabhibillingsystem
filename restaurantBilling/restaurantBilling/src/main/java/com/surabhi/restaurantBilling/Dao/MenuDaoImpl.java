package com.surabhi.restaurantBilling.Dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.surabhi.restaurantBilling.model.Menu;

@Repository
public class MenuDaoImpl implements MenuDao {
	
	private EntityManager entityManager ;
	
	@Autowired
	public MenuDaoImpl(EntityManager theEntityManager ) {
		entityManager = theEntityManager ;
	}
	
	// returns all menu items
	@Override 
	public List<Menu> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Menu> theQuery = currentSession.createQuery("from Menu",Menu.class);
		List<Menu> menu = theQuery.getResultList();
		return menu;
	}
}

