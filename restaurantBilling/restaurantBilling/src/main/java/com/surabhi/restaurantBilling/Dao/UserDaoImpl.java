package com.surabhi.restaurantBilling.Dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.surabhi.restaurantBilling.model.Admin;
import com.surabhi.restaurantBilling.model.User;

@Transactional 
@Repository
public class UserDaoImpl implements UserDao {

	private EntityManager entityManager;
	
	@Autowired
	public UserDaoImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<User> findall() {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		// select * from user => from User => User is a class name
		Query<User> theQuery = currentSession.createQuery("from User",User.class);
		List<User> users = theQuery.getResultList();
		return users;
	}

	@Override
    public void update(User theUser) {
    	Session currentSession = entityManager.unwrap(Session.class); 
    	currentSession.update(theUser);
    }
	
	@Override
    public void save(User theUser) {
    	Session currentSession = entityManager.unwrap(Session.class); 
    	currentSession.save(theUser);
    }
	

	@Override
	public void deleteById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class); 
		
		// create a query
		Query theQuery = currentSession.createQuery("delete from User where id = :userid");
		// set parameters to query
		theQuery.setParameter("userid", theId);
		theQuery.executeUpdate();
	}
	
	@Override
	public boolean isValid(String theName, String thePassword) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		
		Query theQuery = currentSession.createQuery("from User where name = :name and password = :password");
		theQuery.setParameter("name", theName);
		theQuery.setParameter("password", thePassword);
		
		List<Admin> ls = theQuery.getResultList();
		
		if(ls.size()==0) {
			return false;// if size of result is zero, it means no records found with given credentials
		}
		return true;
	}

	//-----returns the user with given id------
		@Override
		public User findById(int theId) {
			Session currentSession = entityManager.unwrap(Session.class); 
			return currentSession.get(User.class, theId);
		}
	
}
