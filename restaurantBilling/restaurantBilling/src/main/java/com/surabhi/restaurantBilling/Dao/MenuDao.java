package com.surabhi.restaurantBilling.Dao;

import java.util.List;
import com.surabhi.restaurantBilling.model.Menu;

public interface MenuDao {

	public List<Menu> findAll();
}
